-------------------------------------------------------------
-- Xyratex CONFIDENTIAL
-- (C) Copyright Xyratex 2007 All rights reserved
-------------------------------------------------------------
-- StarTeam updated fields
-- Project:     $Project:$
-- Filename:    $Workfile:$
-- Revision:    $Revision:$
-- TimeStamp:   $Date:$
-- Author:      $Author:$
-------------------------------------------------------------
-- Description: Timer for the LEDs
-------------------------------------------------------------
-- Log: $Log:
-- Log: $
-------------------------------------------------------------

library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity LEDs_CLK_100MHZ is
	port (
		Clock               : in  std_logic;
		Reset               : in  std_logic;
		LED_out             : out std_logic
        );
end LEDs_CLK_100MHZ;

architecture rtl of LEDs_CLK_100MHZ is
    ------------------------------------------------------------------
    --                     Main Signals
    ------------------------------------------------------------------
    signal Counter                       : std_logic_vector(27 downto 0);
    signal Clock_1HZ                     : std_logic;
    constant Counter_Max_Value           : std_logic_vector(27 downto 0):=X"17D7840"; -- 50MHz -> 1Hz
    ------------------------------------------------------------------
begin
    -------------------------------------------------------------------
    -- Main Counter, Divides the 100MHZ Clock into 1HZ Clock
    -------------------------------------------------------------------
    process(Clock,Reset)
	begin
			if Reset = '1'  then
				Counter	<=(others=>'0');
				Clock_1HZ<='0';	
			elsif rising_edge(Clock) then
                if (Counter=Counter_Max_Value) then
                    Counter	<=(others=>'0');
                    Clock_1HZ<=NOT Clock_1HZ;
                else
                    Counter<=Counter+'1';                               
                end if;       
            end if;
    end process;  
    -- Update the Output
    LED_Out<= Clock_1HZ; 
    -------------------------------------------------------------------
end rtl;




