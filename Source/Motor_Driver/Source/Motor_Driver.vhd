LIBRARY ieee ;
USE ieee.std_logic_1164.all ;
-- USE ieee.numeric_std.all ;
-- USE ieee.std_logic_arith.all ;
-- USE ieee.std_logic_unsigned.all ;


ENTITY Motor_Driver IS
    GENERIC ( n : INTEGER := 14 ) ;
    PORT (
         -- clk             : IN STD_LOGIC ;
         motor_clk       : IN STD_LOGIC ;
         reset           : IN STD_LOGIC ;
         direction       : IN STD_LOGIC ;
         enable          : IN STD_LOGIC ;
         motor_outputs   : OUT STD_LOGIC_VECTOR(3 downto 0) 
         ) ;
END Motor_Driver ;

ARCHITECTURE rtl OF Motor_Driver IS

    signal motor_state : STD_LOGIC_VECTOR(3 downto 0) := "0001" ;
                
BEGIN

    -- Light 7-seg display
    drive_motor : PROCESS ( motor_clk, motor_state, reset )
    BEGIN
        IF reset = '1'  then
				motor_state <= motor_state;
        ELSIF motor_clk'EVENT AND motor_clk = '1' THEN
            -- check for zero counter
            motor_state <= motor_state ; 
            IF enable = '1' THEN 
                IF direction = '1' THEN
                    IF motor_state = "0001" THEN motor_state <= "0011" ; END IF ;
                    IF motor_state = "0011" THEN motor_state <= "0010" ; END IF ;
                    IF motor_state = "0010" THEN motor_state <= "0110" ; END IF ;
                    IF motor_state = "0110" THEN motor_state <= "0100" ; END IF ;
                    IF motor_state = "0100" THEN motor_state <= "1100" ; END IF ;
                    IF motor_state = "1100" THEN motor_state <= "1000" ; END IF ;
                    IF motor_state = "1000" THEN motor_state <= "1001" ; END IF ;
                    IF motor_state = "1001" THEN motor_state <= "0001" ; END IF ;
                ELSE
                    IF motor_state = "0001" THEN motor_state <= "1001" ; END IF ;
                    IF motor_state = "0011" THEN motor_state <= "0001" ; END IF ;
                    IF motor_state = "0010" THEN motor_state <= "0011" ; END IF ;
                    IF motor_state = "0110" THEN motor_state <= "0010" ; END IF ;
                    IF motor_state = "0100" THEN motor_state <= "0110" ; END IF ;
                    IF motor_state = "1100" THEN motor_state <= "0100" ; END IF ;
                    IF motor_state = "1000" THEN motor_state <= "1100" ; END IF ;
                    IF motor_state = "1001" THEN motor_state <= "1000" ; END IF ;
                END IF ;
            END IF ;
        END IF ;
        motor_outputs <= motor_state ;
    END PROCESS ;
END rtl;

