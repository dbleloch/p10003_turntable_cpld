-------------------------------------------------------------
-- Xyratex CONFIDENTIAL
-- (C) Copyright Xyratex 2007 All rights reserved
-------------------------------------------------------------
-- StarTeam updated fields
-- Project:     $Project:$
-- Filename:    $Workfile:$
-- Revision:    $Revision:$
-- TimeStamp:   $Date:$
-- Author:      $Author:$
-------------------------------------------------------------
-- Description: Timer for the LEDs
-------------------------------------------------------------
-- Log: $Log:
-- Log: $
-------------------------------------------------------------

library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity Motor_Clock_Divider is
	port (
		Clock_In            : in  std_logic;
		Reset               : in  std_logic;
		Clock_Out           : out std_logic
        );
end Motor_Clock_Divider;

architecture rtl of Motor_Clock_Divider is
    ------------------------------------------------------------------
    --                     Main Signals
    ------------------------------------------------------------------
    signal Counter                       : std_logic_vector(27 downto 0);
    signal Slow_Clock                     : std_logic;
    constant Counter_Max_Value           : std_logic_vector(27 downto 0):=X"000E100"; 
    -- constant Counter_Max_Value           : std_logic_vector(27 downto 0):=X"0000010"; -- simulation mode only
    ------------------------------------------------------------------
begin
    -------------------------------------------------------------------
    -- Main Counter, Divides the 100MHZ Clock into Slow Clock
    -------------------------------------------------------------------
    process(Clock_In,Reset)
	begin
			if Reset = '1'  then
				Counter	<=(others=>'0');
				Slow_Clock<='0';	
			elsif rising_edge(Clock_In) then
                if (Counter=Counter_Max_Value) then
                    Counter	<=(others=>'0');
                    Slow_Clock<=NOT Slow_Clock;
                else
                    Counter<=Counter+'1';                               
                end if;       
            end if;
    end process;  
    -- Update the Output
    Clock_Out<= Slow_Clock; 
    -------------------------------------------------------------------
end rtl;




