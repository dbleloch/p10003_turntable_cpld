LIBRARY ieee ;
USE ieee.std_logic_1164.all ;
-- USE ieee.numeric_std.all ;
-- USE ieee.std_logic_arith.all ;
-- USE ieee.std_logic_unsigned.all ;

----------------------
-- Top-level module --
----------------------

ENTITY Turntable_Top_Level IS
    GENERIC
        (
        n : INTEGER := 14
        ) ;
        
    PORT
        (
        in_clock                    : IN STD_LOGIC ;
        -- in_resetn                   : IN STD_LOGIC ;
        top_level_motor_outputs     : OUT STD_LOGIC_VECTOR (3 DOWNTO 0) ;
        top_level_led               : OUT STD_LOGIC ;
		  
        top_level_Rx_in             : IN STD_LOGIC ;
        top_level_Tx_out            : OUT STD_LOGIC 
		  
        -- top_level_button_input      : IN STD_LOGIC 
        -- temp_enable_monitor         : OUT STD_LOGIC 
		  
        ) ;
    END Turntable_Top_Level ;

    
ARCHITECTURE rtl OF Turntable_Top_Level IS

    component avalon_sys is
    port (
        clk_clk                             : in  std_logic                     := 'X'; -- clk
        reset_reset_n                       : in  std_logic                     := 'X'; -- reset_n
        uart_master_0_conduit_end_TX        : out std_logic;                            -- TX
        uart_master_0_conduit_end_RX        : in  std_logic                     := 'X'; -- RX
        pio_0_external_connection_export    : out std_logic_vector(31 downto 0)         -- export
        );
        end component avalon_sys;

    component Motor_Driver
    port ( 
        -- Clock inputs and Reset
        motor_clk            : IN STD_LOGIC ;
        direction            : IN STD_LOGIC ;
        enable               : IN STD_LOGIC ;
        reset                : IN STD_LOGIC ;
        motor_outputs        : OUT STD_LOGIC_VECTOR (3 DOWNTO 0) 
        );
		  end component Motor_Driver;
   
    component Motor_Clock_Divider IS
        PORT
        (
            Clock_In         : IN STD_LOGIC ;
            Reset            : IN STD_LOGIC  := '1';
            Clock_Out        : OUT STD_LOGIC
        );
        END component Motor_Clock_Divider;
          
          
    component LEDs_CLK_100MHZ is
         port
          (
          Clock               : in  std_logic;
          Reset               : in  std_logic;
          LED_out             : out std_logic
        );
        end component LEDs_CLK_100MHZ;
         
         
    signal top_level_motor_clock         : std_logic := '0';
    signal top_level_direction           : std_logic := '1';
    signal top_level_enable              : std_logic := '1';
    signal top_level_OK                  : std_logic := '1';
     --==================================
    signal reset                         : std_logic;
	 signal resetn                        : std_logic;
     --==================================
    -- signal top_level_button_input        : std_logic := '1';               
    -- signal top_level_current_location    : std_logic_vector(31 downto 0) := "00001111";
    -- signal top_level_destination         : std_logic_vector(31 downto 0) := "11110000";

    -- signal top_level_AvalonChipSelect    : std_logic;
    -- signal top_level_AvalonAddress       : std_logic_vector(31 downto 0);
    -- signal top_level_AvalonRead          : std_logic;
    -- signal top_level_AvalonReadData      : std_logic_vector(31 downto 0);
    -- signal top_level_AvalonWrite         : std_logic;
    -- signal top_level_AvalonWriteData     : std_logic_vector(31 downto 0);
    -- signal top_level_AvalonWaitRequest   : std_logic;

    signal PIO_signals                   : std_logic_vector(31 downto 0);

BEGIN

    reset <= '0' ;
	 resetn <= '1' ;
	 
    top_level_led <= PIO_signals(0) ;
    
   u_avalon : avalon_sys
      port map (
          clk_clk                             => in_clock,                             --                              clk.clk
          reset_reset_n                       => resetn,                       --                            reset.reset_n
          uart_master_0_conduit_end_TX        => top_level_Tx_out,     -- avalon_master_uart_0_conduit_end.TX
          uart_master_0_conduit_end_RX        => top_level_Rx_in,      --                                 .RX
          pio_0_external_connection_export    => pio_signals           --        pio_0_external_connection.export
      );
  

    u_motor_clock_gen : Motor_Clock_Divider
    port map ( 
        -- Clock inputs and Reset
        Clock_In                         => in_clock,
        Reset                            => reset,
        Clock_Out                        => top_level_motor_clock
        );

        
    u_motor_driver : motor_driver
    port map ( 
        -- Clock inputs and Reset
        motor_clk                      => top_level_motor_clock,
        direction                      => top_level_direction,
        enable                         => top_level_enable,
        reset                          => reset,
        motor_outputs                  => top_level_motor_outputs      
        );
          
          
     u_led : LEDs_CLK_100MHZ
    port map ( 
        -- Clock inputs and Reset
        Clock                         => in_clock,
        Reset                         => reset,
        LED_out                       => open
        );
         
END rtl;

