-------------------------------------------------------------
-- Xyratex CONFIDENTIAL 
-- (C) Copyright Xyratex 2012 All rights reserved
-------------------------------------------------------------
library ieee ;
use ieee.std_logic_1164.all ;
use	ieee.std_logic_unsigned.all;

architecture UART_to_Avalon_MM_Master_arch of UART_to_Avalon_MM_Master is

    component uart_rx is
	Port(	
        Clock                   : in  std_logic;
        Reset                   : in  std_logic;
        AvalonStream_data       : out  std_logic_vector(7 downto 0);
        AvalonStream_valid      : out  std_logic;
        UartClockCount          : in  std_logic_vector(15 downto 0);
        rx                      : in std_logic
	);
    end component uart_rx;
    
    component uart_tx is
	Port(	
        Clock                   : in  std_logic;
        Reset                   : in  std_logic;
        AvalonStream_data       : in  std_logic_vector(7 downto 0);
        AvalonStream_valid      : in  std_logic;
        AvalonStream_ready      : out std_logic;
        UartClockEnable         : in  std_logic;
        TX                      : out std_logic	
	);
    end component uart_tx;
    
    component baud_rate_gen is
    generic (
        baud_counter        : integer
        );
    port (
        Clock                   : in  std_logic;
        Reset                   : in  std_logic;
        UartClockCount          : out std_logic_vector(15 downto 0);
        UartClockEnable         : out std_logic        
    );
    end component baud_rate_gen;
    
    
    component Protocol_Interpreter is
    port (
        Clock   : in std_logic;
        Reset   : in std_logic;
        Tx_Data_o  : out std_logic_vector(7 downto 0);
        Tx_Valid_o : out std_logic;
        Tx_Ready_i : in std_logic;
        Rx_Data_i  : in std_logic_vector(7 downto 0);
        Rx_Valid_i : in std_logic;
        MasterPortAvalonAddress         : out std_logic_vector(31 downto 0); 
        MasterPortAvalonWrite           : out std_logic;                     
        MasterPortAvalonWriteData       : out std_logic_vector(31 downto 0); 
        MasterPortAvalonRead            : out std_logic;
        MasterPortAvalonReadData        : in std_logic_vector(31 downto 0);
        MasterPortAvalonChipSelect      : out std_logic;
        MasterPortAvalonWaitRequest     : in std_logic
    );
    end component Protocol_Interpreter;
    

    signal Tx_Data_o_s       : std_logic_vector(7 downto 0);
    signal Tx_Valid_o_s      : std_logic;
    signal Tx_Ready_i_s      : std_logic;
    signal Rx_Data_i_s       : std_logic_vector(7 downto 0);
    signal Rx_Valid_i_s      : std_logic;
    signal UartClockCount_s  : std_logic_vector(15 downto 0);
    signal UartClockEnable_s : std_logic;

begin
    
    U_uart_rx : uart_rx
	Port map(	
        Clock                   => MasterPortAvalonClock,
        Reset                   => MasterPortAvalonReset,
        AvalonStream_data       => Rx_Data_i_s,
        AvalonStream_valid      => Rx_Valid_i_s,
        UartClockCount          => UartClockCount_s,
        rx                      => Rx
    );
    
    U_uart_tx : uart_tx
	Port map(	
        Clock                   => MasterPortAvalonClock,
        Reset                   => MasterPortAvalonReset,
        AvalonStream_data       => Tx_Data_o_s, 
        AvalonStream_valid      => Tx_Valid_o_s,
        AvalonStream_ready      => Tx_Ready_i_s,
        UartClockEnable         => UartClockEnable_s,
        TX                      => Tx
	);
    
    U_baud_rate_gen : baud_rate_gen
    generic map(
        --baud_counter        => 868 -- 115200 baud @ 100MHz
        baud_counter        => 434 -- 115200 baud @ 50MHz
        )
    port map(
        Clock                   => MasterPortAvalonClock,
        Reset                   => MasterPortAvalonReset,
        UartClockCount          => UartClockCount_s,
        UartClockEnable         => UartClockEnable_s
    );
    
    U_Protocol_Interpreter : Protocol_Interpreter
    port map(
        Clock                      => MasterPortAvalonClock,
        Reset                      => MasterPortAvalonReset,
        Tx_Data_o                  => Tx_Data_o_s  ,
        Tx_Valid_o                 => Tx_Valid_o_s ,
        Tx_Ready_i                 => Tx_Ready_i_s ,
        Rx_Data_i                  => Rx_Data_i_s  ,
        Rx_Valid_i                 => Rx_Valid_i_s ,
        MasterPortAvalonAddress    => MasterPortAvalonAddress     ,
        MasterPortAvalonWrite      => MasterPortAvalonWrite       ,
        MasterPortAvalonWriteData  => MasterPortAvalonWriteData   ,
        MasterPortAvalonRead       => MasterPortAvalonRead        ,
        MasterPortAvalonReadData   => MasterPortAvalonReadData    ,
        MasterPortAvalonChipSelect => MasterPortAvalonChipSelect  ,
        MasterPortAvalonWaitRequest=> MasterPortAvalonWaitRequest 
    );
    
end architecture UART_to_Avalon_MM_Master_arch;