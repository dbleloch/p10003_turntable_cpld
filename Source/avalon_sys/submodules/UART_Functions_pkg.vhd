-------------------------------------------------------------
-- Xyratex CONFIDENTIAL 
-- (C) Copyright Xyratex 2010 All rights reserved
-------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.ALL;
USE ieee.std_logic_arith.all;

package UART_Functions_Package is

    function To_ascii (hex_byte : std_logic_vector)
            return std_logic_vector;
            
end package UART_Functions_Package;

package body UART_Functions_Package is

    function To_ascii (hex_byte : std_logic_vector)
            return std_logic_vector is
        variable hexbyte : std_logic_vector(3 downto 0) := hex_byte;
    begin   
        case hexbyte is
            when x"0" => return x"30"; -- '0'
            when x"1" => return x"31"; -- '1'
            when x"2" => return x"32"; -- '2'
            when x"3" => return x"33"; -- '3'
            when x"4" => return x"34"; -- '4'
            when x"5" => return x"35"; -- '5'
            when x"6" => return x"36"; -- '6'
            when x"7" => return x"37"; -- '7'
            when x"8" => return x"38"; -- '8'
            when x"9" => return x"39"; -- '9'
            when x"A" => return x"41"; -- 'A'
            when x"B" => return x"42"; -- 'B'
            when x"C" => return x"43"; -- 'C'
            when x"D" => return x"44"; -- 'D'
            when x"E" => return x"45"; -- 'E'
            when x"F" => return x"46"; -- 'F'
            when others => return x"21"; --!
        end case;
    end function To_ascii;
	
end package body UART_Functions_Package;