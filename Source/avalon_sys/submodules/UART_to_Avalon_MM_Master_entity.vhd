-------------------------------------------------------------
-- Xyratex CONFIDENTIAL 
-- (C) Copyright Xyratex 2012 All rights reserved
-------------------------------------------------------------
library ieee ;
use ieee.std_logic_1164.all ;

entity UART_to_Avalon_MM_Master is
	-- generic(
		 -- baud_counter        : integer := 868 -- defaults to 115200 with 100MHz sys clock
        -- );
	port(	
    --------- avalon signals -------------
        MasterPortAvalonClock           : in std_logic;
        MasterPortAvalonReset           : in std_logic;

        MasterPortAvalonAddress         : out std_logic_vector(31 downto 0); 
        MasterPortAvalonWrite           : out std_logic;                     
        MasterPortAvalonWriteData       : out std_logic_vector(31 downto 0); 
        MasterPortAvalonRead            : out std_logic;
        MasterPortAvalonReadData        : in std_logic_vector(31 downto 0);
        MasterPortAvalonChipSelect      : out std_logic;
        MasterPortAvalonWaitRequest     : in std_logic;
        
	----------------------------------------------------
        TX     : out std_logic;
		RX	   : in std_logic
		);
end entity;