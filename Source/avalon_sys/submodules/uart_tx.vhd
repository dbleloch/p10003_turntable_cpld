-------------------------------------------------------------
-- Xyratex CONFIDENTIAL 
-- (C) Copyright Xyratex 2012 All rights reserved
-------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.ALL;

entity uart_tx is

	Port(	
        --Misc
        Clock                   : in  std_logic;
        Reset                   : in  std_logic;
        -- Avalon
        AvalonStream_data       : in  std_logic_vector(7 downto 0);
        AvalonStream_valid      : in  std_logic;
        AvalonStream_ready      : out std_logic;
        -- Uart control
        
        UartClockEnable         : in  std_logic;

        -- Uart Interface
        TX                      : out std_logic	
	);
end entity uart_tx;

architecture uart_tx_arch of uart_tx is

    constant START : std_logic := '0';
    constant STOP  : std_logic := '1';
    signal AvalonStream_ready_int : std_logic := '1';
    
begin

    AvalonStream_ready <= '0' when ((AvalonStream_valid = '1') or (AvalonStream_ready_int = '0')) else '1';
    
    process(Clock,Reset)
        variable shift : std_logic_vector(9 downto 0); -- 2 extra bits, 1 start & 1 stop
        variable bitstogo : std_logic_vector(3 downto 0);
    begin
        if (Reset='1') then
            shift := (others=>'1');
            TX <= '1';
            AvalonStream_ready_int <='1';
        elsif rising_edge(Clock) then

            if UartClockEnable = '1' then
                TX <= shift(0);
            end if;


            if AvalonStream_valid='1' then
                AvalonStream_ready_int <='0';
                shift(0) := START; 
                shift(8 downto 1) := AvalonStream_data;
                shift(9) := STOP;
                bitstogo := x"A";
            end if;
        

            if UartClockEnable = '1' then
                if bitstogo /= x"0" then
                    shift := '1' & shift(9 downto 1);
                    bitstogo := bitstogo - '1';
                else
                    AvalonStream_ready_int <='1';
                end if;

            end if;
        
            
        
        end if;
    end process;




end architecture uart_tx_arch;

