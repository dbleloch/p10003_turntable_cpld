-------------------------------------------------------------
-- Xyratex CONFIDENTIAL 
-- (C) Copyright Xyratex 2012 All rights reserved
-------------------------------------------------------------
library ieee ;
use ieee.std_logic_1164.all ;
use	ieee.std_logic_unsigned.all;

library work;
use work.UART_Functions_Package.all;

entity Protocol_Interpreter is
    port (
        Clock   : in std_logic;
        Reset   : in std_logic;
        -- Avalon-ST Source
        Tx_Data_o  : out std_logic_vector(7 downto 0);
        Tx_Valid_o : out std_logic;
        Tx_Ready_i : in std_logic;
        -- Avalon-ST Sink
        Rx_Data_i  : in std_logic_vector(7 downto 0);
        Rx_Valid_i : in std_logic;
        -- Avalon-MM Master
        MasterPortAvalonAddress         : out std_logic_vector(31 downto 0); 
        MasterPortAvalonWrite           : out std_logic;                     
        MasterPortAvalonWriteData       : out std_logic_vector(31 downto 0); 
        MasterPortAvalonRead            : out std_logic;
        MasterPortAvalonReadData        : in std_logic_vector(31 downto 0);
        MasterPortAvalonChipSelect      : out std_logic;
        MasterPortAvalonWaitRequest     : in std_logic
    );
end entity Protocol_Interpreter;

architecture Protocol_Interpreter_arch of Protocol_Interpreter is

    type read_write_t is (f_read, f_write);
    type destination_t is (d_address, d_data);
    type protocol_t is (f_ascii, f_binary);
    type transmission_t is (f_normal, f_incrementing, f_static);
    type Protocol_decode_P_t is (Idle, ASCII_decode, binary_decode, master_the_avalon, read_wait,
                                write_wait_delay, multi_read, next_multi_read, multi_write);
    signal State  : Protocol_decode_P_t;
    signal next_state  : Protocol_decode_P_t;
    
    signal Reset_int : std_logic;
    signal Tx_Data_s : std_logic_vector(31 downto 0);
    signal Tx_Valid_s : std_logic;
    signal Tx_Ready_s : std_logic;
    signal Tx_Ready_s_int : std_logic;
    signal protocol: protocol_t;

begin

    Watchdog_P: process (Clock,Reset) -- watchdog prevents state machine lockup on comms error
        variable watchdog_v : std_logic_vector(26 downto 0) := (others=>'1'); -- 27-bit watchdog = 2.684 secs timeout with 50MHz Clock
        constant zero_v     : std_logic_vector(26 downto 0) := (others=>'0'); -- changed to a constant as initial value is not synthesizable
    begin
        if Reset='1' then
            Reset_int <= '1';
        elsif rising_edge(Clock) then
            if (Rx_Valid_i = '1' or Tx_Valid_s = '1') then
                Reset_int <= '0';
                watchdog_v := (others=>'1');
            elsif watchdog_v = zero_v then
                Reset_int <= '1';
            else
                Reset_int <= '0';
                watchdog_v := watchdog_v - 1;
            end if;
        end if;
    end process Watchdog_P;
    
    
    Protocol_decode_P: process (Clock,Reset_int, Rx_Valid_i)
        variable read_write_flag : read_write_t := f_write;
        variable nibble_counter: std_logic_vector(2 downto 0);
        variable nibble: std_logic_vector(7 downto 0);
        variable destination: destination_t;
        variable transmission_v : transmission_t;
        variable data32 : std_logic_vector(31 downto 0);
        variable s_address : std_logic_vector(31 downto 0) := (others=>'0');
        variable s_data : std_logic_vector(31 downto 0) := (others=>'0');
        variable address_counter : std_logic_vector(15 downto 0);
    
    begin
        if Reset_int='1' and (Rx_Valid_i = '0') then -- modified reset condition, originally it would ignore the first Rx_Valid and there ore comms would time out
            State <= Idle;
            nibble_counter := (others=>'0');
            nibble := (others=>'0');
            destination := d_address;
            transmission_v := f_normal;
            protocol <= f_ascii;
            data32 := (others=>'0');
            address_counter := (others=>'0');
        elsif rising_edge(Clock) then
            case State is
                when Idle =>
                    MasterPortAvalonAddress <= (others => '0');
                    MasterPortAvalonWriteData <= (others => '0');
                    MasterPortAvalonChipSelect <= '0';
                    MasterPortAvalonWrite <= '0';
                    MasterPortAvalonRead  <= '0';
                    Tx_Valid_s <= '0';
                    Tx_Data_s <= (others=>'0');
                    nibble_counter := (others=>'0');
                    next_state <= Idle;
                    
                    if Rx_Valid_i = '1' then -- command character (R/W)
                        case Rx_Data_i is
                            when X"57" => --"W"
                                read_write_flag := f_write;
                                protocol <= f_ascii;
                                state <= ASCII_decode;
                            when X"52" => --"R"
                                read_write_flag := f_read;
                                protocol <= f_ascii;
                                state <= ASCII_decode;
                                
                                
                            when X"4F" => -- read
                                read_write_flag := f_read;
                                protocol <= f_binary;
                                transmission_v := f_normal;
                                state <= binary_decode;
                                next_state <= master_the_avalon;
                            when X"53" => -- static read
                                read_write_flag := f_read;
                                protocol <= f_binary;
                                transmission_v := f_static;
                                State <= binary_decode;
                                next_state <= multi_read;
                            when X"49" => -- incrementing read
                                read_write_flag := f_read;
                                protocol <= f_binary;
                                transmission_v := f_incrementing;
                                State <= binary_decode;
                                next_state <= multi_read;
                            when X"54" => -- write
                                read_write_flag := f_write;
                                protocol <= f_binary;
                                transmission_v := f_normal;
                                state <= binary_decode;
                                next_state <= master_the_avalon;
                            when X"50" => -- static write
                                read_write_flag := f_write;
                                protocol <= f_binary;
                                transmission_v := f_static;
                                State <= binary_decode;
                                next_state <= multi_write;
                            when X"4D" => -- incrementing write
                                read_write_flag := f_write;
                                protocol <= f_binary;
                                transmission_v := f_incrementing;
                                State <= binary_decode;
                                next_state <= multi_write;
                            when others =>
                                state <= idle;
                        end case;
                        
                    end if;
                    
                when ASCII_decode =>
                    if Rx_Valid_i = '1' then
                        if Rx_Data_i(6) = '1' then
                            nibble := Rx_Data_i + 9; -- ASCII_decode ascii A-F
                        elsif Rx_Data_i(5) = '1' then
                            nibble := Rx_Data_i; -- ASCII_decode ascii 0-9
                        else
                            nibble := X"00";
                            State <= idle;
                        end if;
                    end if;
                    
                    if Rx_Valid_i = '1' then
                        data32 := data32(27 downto 0) & nibble(3 downto 0);
                        
                        if nibble_counter = "111" then
                            if destination = d_address then
                                s_address := data32;
                                if read_write_flag = f_read then
                                    State <= master_the_avalon;
                                else
                                    destination := d_data;
                                end if;
                            elsif destination = d_data then
                                s_data := data32;
                                destination := d_address;
                                State <= master_the_avalon;
                            end if;
                        end if;
                        
                        nibble_counter := nibble_counter + 1;
                    end if;
                    
                when binary_decode =>
                    if Rx_Valid_i = '1' then
                        data32 := data32(23 downto 0) & Rx_Data_i;
                        
                        nibble_counter := nibble_counter + 1;
                        if nibble_counter = "100" then
                            if destination = d_address then
                                s_address := data32;
                                if (read_write_flag = f_read and transmission_v = f_normal) then
                                    State <= master_the_avalon;
                                else
                                    destination := d_data;
                                end if;
                            elsif destination = d_data then
                                s_data := data32;
                                destination := d_address;
                                State <= next_state;
                                address_counter := s_data(15 downto 0);
                            end if;
                            nibble_counter := (others=>'0');
                        end if;
                        
                    end if;
                
                when multi_read =>
                    next_state <= next_multi_read;
                    Tx_Valid_s <= '0';
                    MasterPortAvalonAddress <= s_address;
                    MasterPortAvalonWrite <= '0';
                    if Tx_Ready_s = '1' then
                        state <= read_wait;
                        MasterPortAvalonChipSelect <= '1';
                        MasterPortAvalonRead  <= '1';
                    end if;
                    
                when next_multi_read =>
                    if address_counter = x"0000" then
                        state <= idle;
                    else
                        address_counter := address_counter - 1;
                        if transmission_v = f_incrementing then
                            s_address := s_address + 4;
                        end if;
                        state <= multi_read;
                    end if;
                   
                when multi_write =>
                    if address_counter = x"0000" then
                        state <= idle;
                    elsif Rx_Valid_i = '1' then
                        data32 := data32(23 downto 0) & Rx_Data_i;
                        
                        nibble_counter := nibble_counter + 1;
                        if nibble_counter = "100" then
                            s_data := data32;
                            State <= master_the_avalon;
                            nibble_counter := (others=>'0');
                            address_counter := address_counter - 1;
                            
                        end if;
                        
                    end if;
                    
                    
                when master_the_avalon =>
                    MasterPortAvalonAddress <= s_address;
                    MasterPortAvalonWriteData <= s_data;
                    MasterPortAvalonChipSelect <= '1';
                    if read_write_flag = f_write then
                        MasterPortAvalonWrite <= '1';
                        MasterPortAvalonRead  <= '0';
                    else
                        MasterPortAvalonWrite <= '0';
                        MasterPortAvalonRead  <= '1';
                    end if;
                    
                    if transmission_v = f_incrementing then
                        s_address := s_address + 4;
                    end if;
                    
                    if read_write_flag = f_write then -- write
                        State <= write_wait_delay;
                    else
                        State <= read_wait;
                    end if;
                    
                when write_wait_delay =>
                    if MasterPortAvalonWaitRequest = '0' then
                        MasterPortAvalonChipSelect <= '0';
                        MasterPortAvalonWrite <= '0';
                        if transmission_v = f_normal then
                            State <= idle;
                        else
                            State <= next_state;
                        end if;
                    end if;
                
                when read_wait =>
                    if MasterPortAvalonWaitRequest = '0' then
                        MasterPortAvalonChipSelect <= '0';
                        MasterPortAvalonRead <= '0';
                        if transmission_v = f_normal then
                            State <= idle;
                        else
                            State <= next_state;
                        end if;
                        Tx_Data_s <= MasterPortAvalonReadData;
                        Tx_Valid_s <= '1';
                    end if;
                    
                when others =>
                    State <= idle;
            end case;
            
        end if;
        
    end process Protocol_decode_P;
    
    Tx_Ready_s <= '0' when ((Tx_Valid_s = '1') or (Tx_Ready_s_int = '0')) else '1';
    
    Tx_control_P: process (Clock,Reset)
        variable Tx_Data_v : std_logic_vector(31 downto 0) := (others=>'0');
        variable byte_counter_v : std_logic_vector(3 downto 0) := (others=>'0');
    begin
        if Reset='1' then
            Tx_Data_o <= (others=>'0');
            Tx_Valid_o <= '0';
            Tx_Ready_s_int <= '1';
        elsif rising_edge(Clock) then
            Tx_Valid_o <= '0'; -- default state
            if byte_counter_v /= x"0" then
                if Tx_Ready_i = '1' then
                    if protocol = f_ascii then
                        Tx_Data_o <= To_ascii(Tx_Data_v(31 downto 28)); -- MSB first
                        Tx_Data_v := Tx_Data_v(27 downto 0) & x"0";
                    elsif protocol = f_binary then
                        Tx_Data_o <= Tx_Data_v(31 downto 24);
                        Tx_Data_v := Tx_Data_v(23 downto 0) & x"00";
                    end if;
                    Tx_Valid_o <= '1';
                    byte_counter_v := byte_counter_v - 1;
                end if;
                
            elsif Tx_Valid_s = '1' then
                Tx_Ready_s_int <= '0';
                Tx_Data_v := Tx_Data_s;
                if protocol = f_ascii then
                    byte_counter_v := x"8";
                elsif protocol = f_binary then
                    byte_counter_v := x"4";
                end if;
            else
                Tx_Ready_s_int <= '1';
            end if;
        end if;
        
    end process Tx_control_P;
    
end architecture Protocol_Interpreter_arch;
